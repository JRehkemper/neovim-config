
-- Set Leaderkey
vim.keymap.set("n", " ", "<Nop>", { silent = true, remap = false })
vim.g.mapleader = " "


-- Setup Whichkey
local wk = require("which-key")
wk.register({
    -- Telescope
    f = {
        name = "file",
        f = { "<cmd>Telescope find_files<cr>", "Find File" },
        g = { "<cmd>Telescope live_grep<cr>", "Grep in Files" },
        b = { "<cmd>Telescope buffers<cr>", "Find Buffers" },
        h = { "<cmd>Telescope help_tags<cr>", "Help Tags" },
    },
    -- Trouble
    t = {
        name = "Trouble",
        t = { "<cmd>TroubleToggle<cr>", "TroubleToggle" },
    },
    -- Nvim-Tree
    name = "NvimTree",
    e = {
        name = "Nvim-Tree",
        --e = { "<cmd>NvimTreeToggle<cr>", "Open NvimTree" },
        "<cmd>NvimTreeToggle<cr>", "Open NvimTree",
    },
    -- Tabs
    b = {
        name = "Buffers",
        c = { "<cmd>:tabnew<cr>", "Create Tab" },
        n = { "<cmd>:tabNext<cr>", "Next Tab" },
        p = { "<cmd>:tabprevious<cr>", "Previouse Tab" },
    },
    -- Splits
    s = {
        name = "Splits",
        v = { "<cmd>vsplit<cr>", "Vertical Split" },
        h = { "<cmd>split<cr>", "Horizontal Split" },
    },
},
{
    prefix = "<leader>" 
})


