
require("lazy").setup({
        -- Catppuccin
        -- Colorscheme
        {
                "catppuccin/nvim",
                name = "catppuccin",
                priority = 1000
        },
        -- Whichkey
        -- Display keybindings
        {
            "folke/which-key.nvim",
            event = "VeryLazy",
            init = function()
                vim.o.timeout = true
                vim.o.timeoutlen = 300
            end,
            opts = {
                -- Custom configuration for whichkey
            }
        },
        -- Telescope
        -- Find files
        {
            "nvim-telescope/telescope.nvim",
            branch = '0.1.x',
            dependencies = { "nvim-lua/plenary.nvim" }
        },
        -- Mason
        -- Manage Language Servers
        {
            {
                "williamboman/mason.nvim",
                opts = {
                    ensure_installed = {
                        "pyright",
                        "gopls",
                        "typescript-language-server",
                        "sumneko_lua",
                        "tailwindcss-language-server",
                    }
                },
            },
            {
                "williamboman/mason-lspconfig.nvim",
                -- config = function()
                --    require("configs.lspconfig")
                --end,
            },
            "neovim/nvim-lspconfig",
        },
        -- LSP Zero
        -- Language Support
        {'VonHeikemen/lsp-zero.nvim', branch = 'v3.x'},
        {'hrsh7th/cmp-nvim-lsp'},
        {'hrsh7th/nvim-cmp'},
        {'L3MON4D3/LuaSnip'},
        -- Trouble
        -- Display more info in errors in code
        {
            "folke/trouble.nvim",
            dependencies = { "nvim-tree/nvim-web-devicons" },
            opts = {
                -- Custom configuration
            },
        },
        -- nvim-tree
        -- File Explorer
        {
            "nvim-tree/nvim-tree.lua",
            version = "*",
            lazy = false,
            dependencies = {
                "nvim-tree/nvim-web-devicons",
            },
            config = function()
                require("nvim-tree").setup {}
            end,
        },

})

local lsp_zero = require('lsp-zero')
lsp_zero.on_attach(function(client, bufnr)
  -- see :help lsp-zero-keybindings
  -- to learn the available actions
  lsp_zero.default_keymaps({buffer = bufnr})
end)

require("mason").setup()
require("mason-lspconfig").setup({
    ensure_installed = {},
    handlers = {
        lsp_zero.default_setup,
  },
})
