-- Set Relative Linenumbers
vim.opt.nu = true
vim.opt.relativenumber = true

-- Set Tabstops to 4
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

-- Enable Auto Indenting
vim.opt.smartindent = true

-- Keep distance to screen border while scrolling
vim.opt.scrolloff = 8

-- Use System Clipboard
vim.opt.clipboard = "unnamedplus"
